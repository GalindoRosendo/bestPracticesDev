# **Version Control System**

Documentacion para manejo de git, gitlab y runners.

## Mejores practicas para integracion continua

Estos son los lineamientos para una buena integracion continua usando git, git flow y gitlab runners

* Al subir cambios a develop, se actualizan todas las cadenas de conexion o rutas de webapis apuntando a dev no a deb u otra rama.
* Al subir cambios en rama release se apuntan hacia QA
* Al subir cambios en rama master, se apuntan hacia Produccion

### Teoria
### Contexto
### Implementacion