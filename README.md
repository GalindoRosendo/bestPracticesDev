# **Best Practices**

La informacion de cada punto se mostrara con la siguiente organizacion:
* Teoria, el marco teorico de la  tecnologia o herramienta en cuestion.
* Contexto, la manera en que dicha tecnologia o herramienta se relaciona con la empresa.
* Implementacion, ejemplo codificado de uno o mas casos utilizando un escenario real en la empresa.

Listado de mejores practicas a la hora de programar, versionar codigo, crear scripts de SQL, etc.
* [Version Control System](./VersionControlSystem.md) (Git, Gitlab, Runners)
* [Data Bases](./Data.md) (SQL Server, MongoDB)
* [FrontEnd (WebApps)](./FrontEnd.md) (Angular, TypeScript)
* [BackEnd (WebAPIs)](./BackEnd.md) (.Net Framework, EntityFramework, C#)


