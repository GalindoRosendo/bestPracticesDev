# **Desarrollo de Zeus**

Estos son los lineamientos para programacion en frontend y backend. 
**Se esta desarrollando la documentacion, en una proxima edicion se agregaran imagenes.** *Tomar en cuenta los siguientes puntos: informacion de trello, imagenes hechas para techradar, it framework*

## **BackEnd**

A continuacion se lista de manera general: 
* Lenguajes
* Nomenclatura para codigo (variables, metodos, regiones, etc.)
* Estructura de aplicaciones o WebAPIs (archivos, carpetas, soluciones en IDEs)
* Implementacion de logica de negocio (aplicaciones web o nativas)
* Tecnologias para extender funcionalidad de las aplicaciones (seguridad, RESTClients, unti testing, `versionamiento de codigo, integracion continua` ) *Los ultimos puntos se van a profundizar en otro documento*


## Lenguajes
Por ahora los lenguajes para programacion son:
* C#
### Teoria
### Contexto
### Implementacion

## Nomenclatura para codigo
Es imporntante manejar un estandar para la organizacion del codigo, nombrado de variables, metodos y regiones, pues se agiliza la depuracion de errores.
### Teoria
En el nombrado de los servicios no se utilizan los nombres de metodos http
se utiliza la nomenclatura CRUD (create, read, update, delete)
### Contexto
### Implementacion


## Estructura de aplicaciones o WebAPIs

La organizacion esta basada en capas de servicios, especificamente desde la inferior *(Data access objects)* hasta la tercer capa o logica de negocio *(Business services)* son librerias (.dlls), posteriormente se consumen por las WebAPIs *(Endpoints)*

### Teoria
Las capas de servicios son utilizadas para segmentar la logica de aplicaciones con grandes estructuras y codigo fuente. Se van a manejar las siguientes:
* Data Access Objects *todo lo relacionado con bases de datos manejado en codigo (entidades de SQL convertidas a codigo)*
* Business Objects *Clases implicadas, es un mapeo de las entidades anteriores con distinto nombre y nomenclatura*
* Business Services *Logica de negocio, aqui es donde se agregan las pre condiciones, post condiciones, errores o excepciones a nivel de codigo*
* Presentation  *Es la manera de consumir los business services, puede ser desde una aplicacion nativa o una WebAPI*

### Contexto
### Implementacion

## Implementacion de logica de negocio

### Teoria
### Contexto
### Implementacion

## Tecnologias para extender funcionalidad de las aplicaciones

### Teoria
### Contexto
### Implementacion
